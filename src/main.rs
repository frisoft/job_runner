extern crate rayon;
use rayon::iter::IntoParallelRefIterator;
use rayon::prelude::ParallelIterator;
use std::process::Command;

const NUM_JOBS: i32 = 33000;
// const NUM_THREADS: usize = 32;

fn main() {
    // let pool = rayon::ThreadPoolBuilder::new()
    //     .num_threads(NUM_THREADS)
    //     .build()
    //     .unwrap();
    // let results = pool.install(|| run_jobs(NUM_JOBS));
    let results = run_jobs(NUM_JOBS);
    println!("Results: {:?}", results);
}

fn run_jobs(num_jobs: i32) -> Vec<i32> {
    let jobs: Vec<_> = (1..num_jobs).collect();
    let results: Vec<_> = jobs.par_iter().map(|x| command(*x)).collect();
    results
}

fn command(n: i32) -> i32 {
    let output = Command::new("./command.sh")
        // .args(&["1"])
        // .args(&["-c", "echo hello", &format!("{}", n)])
        .output()
        .expect("failed to execute process");
    println!("Job {}", n);
    output.status.code().unwrap_or(-1)
}
